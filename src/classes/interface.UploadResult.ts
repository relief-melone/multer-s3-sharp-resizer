import { Writable } from 'stream';

export default interface UploadResult {
  stream: Writable,
  upload: Promise<any>
}
import { Request } from 'express';

type Metadata = (req: Request, file: Express.Multer.File) => Record<string,any> | Promise<Record<string,any>>;

export default Metadata;
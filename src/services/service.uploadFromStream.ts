import UploadInput from '../classes/interface.UploadInput';
import GetFilenameBase from '../classes/type.GetFilenameBase';
import { Request } from 'express';
import Metadata from '../classes/type.Metadata';
import UploadResult from '../classes/interface.UploadResult';
import { PassThrough } from 'stream';
import path from 'path';
import getDefaultFilenameBase from './service.getDefaultFilenameBase';


export default async (
  req:Request, 
  input: UploadInput,
  getFilenameBase:GetFilenameBase = getDefaultFilenameBase, 
  metadata:Metadata
):Promise<UploadResult> => {

  if (!input.key && !input.file)
    throw new Error('No Key or function for key generation was provided!');

  const stream = new PassThrough();
  const upload = input.s3.upload({
    Bucket: input.bucket,
    Key: input.key || `${getFilenameBase(req, input.file)}${path.extname(input.file!.originalname)}`,
    Body: stream,
    Metadata: await metadata(req, input.file)
  }).promise();

  return {
    upload,
    stream
  };
};
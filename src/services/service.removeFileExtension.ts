export default (completeFilename: string):string => 
  completeFilename.split('.').slice(0, -1).join('.');
import sharp from 'sharp';
import { Writable } from 'stream';
import ImageConversionType from '../classes/interface.ImageConversionType';

export default (input:ImageConversionType):Writable =>{  
  const stream = sharp()
    .resize(input.resize || {});
    

  switch (input.fileFormat){
    case 'png':
      return stream.png();
    case 'webp':
      return stream.webp();
    case 'jpg':
      return stream.webp();
    default:
      throw new Error('Unkown file type');
  }
};
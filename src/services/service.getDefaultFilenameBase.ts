import crypto from 'crypto';

export default ():string =>
  crypto.randomBytes(16).toString('hex');